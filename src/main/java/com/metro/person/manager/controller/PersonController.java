package com.metro.person.manager.controller;

import com.metro.person.manager.entity.Person;
import com.metro.person.manager.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PersonController {
    @Autowired
    PersonService personService;

    @GetMapping("/person/{id}")
    public Person getPerson(@PathVariable("id")Integer personId){
        return personService.getPerson(personId);
    }

    @PostMapping("/person")
    public @ResponseBody Person createPerson(@RequestBody Person person){
        return personService.createPerson(person);
    }

    @PutMapping("/person")
    public Person updatePerson(Person personId){
        return null;
    }

    @DeleteMapping("/person/{id}")
    public void deletePerson(@PathVariable("id")Integer personId){

    }
}
