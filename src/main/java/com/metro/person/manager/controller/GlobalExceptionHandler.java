package com.metro.person.manager.controller;

import com.metro.person.manager.service.impl.PersonServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleException(RuntimeException exception) {
        log.error(exception.getMessage());

        if (exception instanceof PersonServiceImpl.InvalidPersonInfo) {
            return new ResponseEntity<>("Invalid person info", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("There's an internal error. Please try again later!", HttpStatus.INTERNAL_SERVER_ERROR);

    }
}
