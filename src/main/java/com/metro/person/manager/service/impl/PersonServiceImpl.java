package com.metro.person.manager.service.impl;

import com.metro.person.manager.entity.Person;
import com.metro.person.manager.repo.PersonRepository;
import com.metro.person.manager.service.PersonService;
import com.metro.person.manager.service.validator.PersonInfoValidationImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    PersonRepository repo;
    @Autowired
    PersonInfoValidationImpl personInfoValidation;

    @Override
    @Transactional(readOnly=true)
    public Person getPerson(Integer id) {
        return repo.findById(id).get();
    }

    @Override
    @Transactional
    public Person createPerson(Person person) {
        String rs = personInfoValidation.validatePhoneFormat(person.getPhone()).block();
        if(rs.contains("invalid")){
            throw new InvalidPersonInfo(rs);
        }
        rs = personInfoValidation.validatePhoneLength(person.getPhone()).block();
        if(rs.contains("invalid")){
            throw new InvalidPersonInfo(rs);
        }
        return repo.save(person);
    }

    @Override
    public void deletePerson(Integer id) {

    }

    @Override
    @Transactional(readOnly=true)
    public List<Person> findAllPerson() {
        return repo.findAll();
    }

    @Override
    public Person updatePerson(Person person) {
        return null;
    }

    public static class InvalidPersonInfo extends RuntimeException {
        public InvalidPersonInfo(String message) {
            super(message);
        }
    }
}
