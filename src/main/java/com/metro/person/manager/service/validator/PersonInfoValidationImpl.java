package com.metro.person.manager.service.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;

@Service
public class PersonInfoValidationImpl {
    private final WebClient client;

    public PersonInfoValidationImpl(@Value("${validation-service.port}") int port) {
        final String url = String.format("http://localhost:%s", port);

        client=WebClient.create(url);
    }

    public Mono<String> validatePhoneFormat(String phone) {
        String url="api2/v2/validate";
        MultiValueMap<String, String> map=new LinkedMultiValueMap<>();
        map.add("phone", phone);

        return invokeValidationService(url, map);
    }

    public Mono<String> validatePhoneLength(String phone) {
        String url="api2/v1/validate";
        MultiValueMap<String, String> map=new LinkedMultiValueMap<>();
        map.add("phone", phone);

        return invokeValidationService(url, map);
    }

    private Mono<String> invokeValidationService(String url, MultiValueMap<String, String> params) {
        return client.get()
                .uri(uriBuilder -> uriBuilder.path(url).queryParams(params).build())
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class).
                        flatMap(body -> Mono.error(new ValidationServiceInvocationException(body, clientResponse.rawStatusCode()))))
                .bodyToMono(String.class)
                .retryWhen(Retry.backoff(3, Duration.ofSeconds(5)).jitter(0.5)
                        .filter(throwable -> throwable instanceof ValidationServiceInvocationException)
                        .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> {
                            throw new ValidationServiceInvocationException("External Service failed to process after max retries", HttpStatus.SERVICE_UNAVAILABLE.value());
                        }));
    }

    public static class ValidationServiceInvocationException extends RuntimeException {
        public final int statusCode;

        public ValidationServiceInvocationException(String message, int statusCode) {
            super(message);
            this.statusCode=statusCode;
        }
    }
}
