package com.metro.person.manager.service.validator;

import com.metro.person.manager.entity.Person;
import com.metro.person.manager.service.impl.PersonServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Slf4j
public class PersonInfoValidator implements Validator {
    @Autowired
    PersonInfoValidationImpl personInfoValidation;

    @Override
    public boolean supports(Class<?> clazz) {
        return Person.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        Person newPerson =(Person) target;
        log.info("BEGIN VALIDATE USER INFO " + newPerson.getName());

        String rs = personInfoValidation.validatePhoneFormat(newPerson.getPhone()).block();
        if(rs.contains("invalid")){
            throw new PersonServiceImpl.InvalidPersonInfo(rs);
        }
        rs = personInfoValidation.validatePhoneLength(newPerson.getPhone()).block();
        if(rs.contains("invalid")){
            throw new PersonServiceImpl.InvalidPersonInfo(rs);
        }
    }
}
