package com.metro.person.manager.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CollectTask {
    //@Scheduled(cron="*/30 * * * * *")
    public void report(){
        log.info("DO THE SCHEDULED JOB");
    }
}
