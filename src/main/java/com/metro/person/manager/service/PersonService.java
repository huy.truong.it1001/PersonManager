package com.metro.person.manager.service;

import com.metro.person.manager.entity.Person;

import java.util.List;


public interface PersonService {
    public Person getPerson(Integer id);
    public Person createPerson(Person person);
    public void deletePerson(Integer id);
    public Person updatePerson(Person person);
    public List<Person> findAllPerson();
}
