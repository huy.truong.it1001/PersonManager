package com.metro.person.manager.repo;

import com.metro.person.manager.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path="person", collectionResourceRel="person")
public interface PersonRepository extends JpaRepository<Person, Integer> {
    @Query("select p from Person p ")
    List<Person> findAll();
}
