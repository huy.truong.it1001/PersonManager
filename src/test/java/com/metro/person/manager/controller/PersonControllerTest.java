package com.metro.person.manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.metro.person.manager.entity.Person;
import com.metro.person.manager.service.PersonService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(controllers=PersonController.class)
public class PersonControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PersonService personService;
    @Autowired
    ObjectMapper mapper;
    @Value("${baseApiUrl}")
    String path;

    @Test
    @SneakyThrows
    public void createPersonTest(){
        Person newPerson = new Person();
        newPerson.setName("Martin Luther");
        newPerson.setEmail("luther@gmail.com");
        newPerson.setPhone("0123211111");

        Person createdPerson = new Person();
        createdPerson.setId(1);
        createdPerson.setName("Martin Luther");
        createdPerson.setEmail("luther@gmail.com");
        createdPerson.setPhone("0123211111");
        when(personService.createPerson(any())).thenReturn(createdPerson);

        RequestBuilder request=MockMvcRequestBuilders.post(path)
                .content(mapper.writeValueAsString(newPerson))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult rs = mockMvc.perform(request)
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().json(res))
                .andReturn();
        System.out.println(rs.getRequest().getPathInfo());
        System.out.println(rs.getResponse().getContentAsString());
    }

    String res = "{\n" +
            "    \"id\": 1,\n" +
            "    \"name\": \"Martin Luther\",\n" +
            "    \"email\": \"luther@gmail.com\",\n" +
            "    \"phone\": \"0123211111\"\n" +
            "}";

}
