package com.metro.person.manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.metro.person.manager.entity.Person;
import lombok.SneakyThrows;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.IOException;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonApiFullTest {
    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();

    private static MockWebServer mockWebServer;
    @BeforeAll
    static void setupMockServer() throws IOException {
        mockWebServer=new MockWebServer();
        mockWebServer.start(8080);

        System.out.println(mockWebServer.getPort());
        System.out.println(mockWebServer.getHostName());
    }
    @AfterAll
    @SneakyThrows
    static void shutdownMockServer() {
        mockWebServer.shutdown();
    }

    void initValidationResponse(String phone){
        String bodyResponse=String.format("The phone %s is valid!", phone);
        mockWebServer.enqueue(new MockResponse()
                .setBody(bodyResponse));
    }

    @Test
    @SneakyThrows
    public void createPersonTest(){
        Person p = new Person();
        p.setName("Martin Luther");
        p.setEmail("luther@gmail.com");
        p.setPhone("0123211111");

        Person createdPerson = new Person();
        createdPerson.setId(1);
        createdPerson.setName("Martin Luther");
        createdPerson.setEmail("luther@gmail.com");
        createdPerson.setPhone("0123211111");

        //Add two responses (/api2/v1/validate, /api2/v2/validate) for ValidationService
        initValidationResponse(p.getPhone());
        initValidationResponse(p.getPhone());

        String path = "/person";
        RequestBuilder request=MockMvcRequestBuilders.post(path)
                .content(mapper.writeValueAsString(p))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult rs = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(createdPerson)))
                .andReturn();
        System.out.println(rs.getResponse().getContentAsString());

    }

}
