package com.metro.person.manager.service;

import com.metro.person.manager.service.validator.PersonInfoValidationImpl;
import lombok.SneakyThrows;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class PersonInfoValidationTest {
    private static MockWebServer mockWebServer;

    @Autowired
    PersonInfoValidationImpl personInfoValidationService;

    @TestConfiguration
    static class PersonInfoValidationTestConfiguration {
        @Bean
        public PersonInfoValidationImpl personInfoValidation() {
            return new PersonInfoValidationImpl(8080);
        }
    }

    @BeforeAll
    static void setupMockServer() throws IOException {
        mockWebServer=new MockWebServer();
        mockWebServer.start(8080);

        System.out.println(mockWebServer.getPort());
        System.out.println(mockWebServer.getHostName());
    }

    @AfterAll
    @SneakyThrows
    static void shutdownMockServer() {
        mockWebServer.shutdown();
    }

    @Test
    @SneakyThrows
    public void testPhoneApiV1() {
        String phone="0726777000";
        String bodyResponse=String.format("The phone %s is valid!", phone);

        mockWebServer.enqueue(new MockResponse()
                .setBody(bodyResponse));

        Mono<String> rs=personInfoValidationService.validatePhoneLength(phone);
        StepVerifier.create(rs).expectNextMatches(s -> s.equals(bodyResponse)).verifyComplete();

        RecordedRequest recordedRequest=mockWebServer.takeRequest();
        assertEquals("GET", recordedRequest.getMethod());
        assertEquals("/api2/v1/validate" + "?phone=" + phone, recordedRequest.getPath());
    }

    @Test
    @SneakyThrows
    public void testPhoneApiV1_5xxError() {
        String phone="0726777000";
        String bodyResponse="The server had an unexpected problem please try again!";

        MockResponse response=new MockResponse()
                .setBody(bodyResponse).setResponseCode(HttpStatus.SERVICE_UNAVAILABLE.value());
        mockWebServer.enqueue(response);
        mockWebServer.enqueue(response);
        mockWebServer.enqueue(response);
        mockWebServer.enqueue(response);

        Mono<String> rs=personInfoValidationService.validatePhoneLength(phone);
        StepVerifier.create(rs).expectErrorMatches(err -> err instanceof PersonInfoValidationImpl.ValidationServiceInvocationException
                && err.getMessage().equals("External Service failed to process after max retries")
                && ((PersonInfoValidationImpl.ValidationServiceInvocationException) err).statusCode == HttpStatus.SERVICE_UNAVAILABLE.value())
                .verify();

        assertEquals(mockWebServer.getRequestCount(), 4);
    }
}
