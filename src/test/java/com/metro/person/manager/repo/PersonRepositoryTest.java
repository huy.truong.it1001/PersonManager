package com.metro.person.manager.repo;

import com.metro.person.manager.config.DBTestConfiguration;
import com.metro.person.manager.entity.Person;
import com.metro.person.manager.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static com.vladmihalcea.sql.SQLStatementCountValidator.*;

@Import(DBTestConfiguration.class)
//@DataJpaTest
@SpringBootTest
public class PersonRepositoryTest {
    @Autowired
    PersonRepository repo;
    @Autowired
    PersonService personService;
    @Autowired
    EntityManager em;

    @Test
    @Transactional
    void createPersonTest(){
        Person newPerson = new Person();
        newPerson.setName("Martin Luther");
        newPerson.setEmail("luther@gmail.com");
        newPerson.setPhone("0123211111");

        Person createdPerson = repo.save(newPerson);

        Assertions.assertEquals(createdPerson.getId(), 1);
        Assertions.assertEquals(createdPerson.getName(), "Martin Luther");
        Assertions.assertEquals(createdPerson.getEmail(), "luther@gmail.com");
        Assertions.assertEquals(createdPerson.getPhone(), "0123211111");
    }

    @Test
    void createPersonQueryTest(){
        reset();

        Person newPerson = new Person();
        newPerson.setName("Martin Luther");
        newPerson.setEmail("luther@gmail.com");
        newPerson.setPhone("0123211111");

        repo.save(newPerson);
        repo.getById(1);
        repo.findById(1);
        repo.findAll();
        personService.findAllPerson();

        assertInsertCount(1);
        assertSelectCount(1);
    }
}
